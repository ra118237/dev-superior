package br.com.william.devsuperior.devvendas.dto;

import java.io.Serializable;
import java.time.LocalDate;

import br.com.william.devsuperior.devvendas.model.Sales;
import br.com.william.devsuperior.devvendas.model.Seller;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SalesDTO implements Serializable{
	private Long id;
	private Integer visited;
	private Integer deals;
	private Double amount;
	private LocalDate date;
	private SellerDTO seller;
	
	public SalesDTO(Sales sales) {
		id = sales.getId();
		visited = sales.getVisited();
		deals = sales.getDeals();
		amount = sales.getAmount();
		date = sales.getDate();
		seller = new SellerDTO(sales.getSeller());
	}
	
}
