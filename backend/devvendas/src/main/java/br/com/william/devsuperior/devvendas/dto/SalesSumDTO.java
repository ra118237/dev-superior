package br.com.william.devsuperior.devvendas.dto;

import java.io.Serializable;
import java.time.LocalDate;

import br.com.william.devsuperior.devvendas.model.Seller;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SalesSumDTO implements Serializable {
	private String nome;
	private Double sum;
	
	public SalesSumDTO(Seller seller, Double sum) {
		nome = seller.getName();
		this.sum = sum;
	}
	
	
}
