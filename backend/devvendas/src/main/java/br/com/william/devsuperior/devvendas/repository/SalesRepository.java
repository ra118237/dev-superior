package br.com.william.devsuperior.devvendas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.william.devsuperior.devvendas.dto.SalesSucessSumDTO;
import br.com.william.devsuperior.devvendas.dto.SalesSumDTO;
import br.com.william.devsuperior.devvendas.model.Sales;

public interface SalesRepository extends JpaRepository<Sales, Long>{
	@Query("SELECT new br.com.william.devsuperior.devvendas.dto.SalesSumDTO(obj.seller, SUM(obj.amount)) "
			+ "FROM Sales AS obj GROUP BY obj.seller")
	List<SalesSumDTO> amountGroupedBySeller();
	
	@Query("SELECT new br.com.william.devsuperior.devvendas.dto.SalesSucessSumDTO(obj.seller, SUM(obj.visited), SUM(obj.deals)) "
			+ " FROM Sales AS obj GROUP BY obj.seller")
	List<SalesSucessSumDTO> successGroupedBySeller();
}
