package br.com.william.devsuperior.devvendas.endpoint;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.william.devsuperior.devvendas.service.SellerService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/sellers")
@RequiredArgsConstructor
public class SellerController {
	private final SellerService sellerService;
	
	@GetMapping
	public ResponseEntity<?> findAll(){
		return ResponseEntity.ok(sellerService.findAll());
	}
}
