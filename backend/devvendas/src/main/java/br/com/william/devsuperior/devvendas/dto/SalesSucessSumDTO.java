package br.com.william.devsuperior.devvendas.dto;

import java.io.Serializable;
import java.time.LocalDate;

import br.com.william.devsuperior.devvendas.model.Seller;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SalesSucessSumDTO implements Serializable {

	private String nome;
	private Long visited;
	private Long deals;
	
	public SalesSucessSumDTO(Seller seller, Long visited, Long deals) {
		nome = seller.getName();
		this.visited = visited;
		this.deals = deals;
	}
}
