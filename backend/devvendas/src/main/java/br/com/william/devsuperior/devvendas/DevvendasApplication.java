package br.com.william.devsuperior.devvendas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class DevvendasApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevvendasApplication.class, args);
	}

}
