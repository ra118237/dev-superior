package br.com.william.devsuperior.devvendas.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.william.devsuperior.devvendas.dto.SalesDTO;
import br.com.william.devsuperior.devvendas.dto.SalesSucessSumDTO;
import br.com.william.devsuperior.devvendas.dto.SalesSumDTO;
import br.com.william.devsuperior.devvendas.model.Sales;
import br.com.william.devsuperior.devvendas.repository.SalesRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SalesService {
	private SalesRepository salesRepository;
	
	@Transactional
	public Page<SalesDTO> findAll(Pageable page){
		Page<Sales> list = salesRepository.findAll(page);
		return list.map(s -> new SalesDTO(s));
	}
	
	public List<SalesSumDTO> amountGroupedBySeller(){
		return salesRepository.amountGroupedBySeller();
	}
	
	public List<SalesSucessSumDTO> successGroupedBySeller(){
		return salesRepository.successGroupedBySeller();
	}
}
