package br.com.william.devsuperior.devvendas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.william.devsuperior.devvendas.model.Seller;

public interface SellerRepository extends JpaRepository<Seller, Long>{

}
