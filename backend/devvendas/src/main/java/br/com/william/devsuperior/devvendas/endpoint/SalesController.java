package br.com.william.devsuperior.devvendas.endpoint;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.william.devsuperior.devvendas.service.SalesService;
import br.com.william.devsuperior.devvendas.service.SellerService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/sales")
@RequiredArgsConstructor
public class SalesController {
	private final SalesService salesService;
	
	@GetMapping
	public ResponseEntity<?> findAll(Pageable pageable){
		return ResponseEntity.ok(salesService.findAll(pageable));
	}
	
	@GetMapping(value = "/sum-by-seller")
	public ResponseEntity<?> amountGroupedBySeller(){
		return ResponseEntity.ok(salesService.amountGroupedBySeller());
	}
	
	@GetMapping(value = "/success-by-seller")
	public ResponseEntity<?> successGroupedBySeller(){
		return ResponseEntity.ok(salesService.successGroupedBySeller());
	}
}
