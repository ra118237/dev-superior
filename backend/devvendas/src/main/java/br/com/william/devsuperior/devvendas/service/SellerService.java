package br.com.william.devsuperior.devvendas.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.william.devsuperior.devvendas.dto.SellerDTO;
import br.com.william.devsuperior.devvendas.model.Seller;
import br.com.william.devsuperior.devvendas.repository.SellerRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SellerService {
	private SellerRepository sellerRepository;
	
	public List<SellerDTO> findAll(){
		List<Seller> list = sellerRepository.findAll();
		return list.stream()
				.map(s -> new SellerDTO(s))
				.collect(Collectors.toList());
	}
}
