

import NavBar from './components/NavBar';
import BarChart from './components/BarChart';
import Footer from './components/Footer';
import DataTable from './components/DataTable';
import DonutChart from './components/DonutChart';

function App() {
  return (
    <>
      <NavBar />
      <div className="container">
        <h1 className="text-primary py-3">Dashboard</h1>
        
        <div className="row ">
          <div className="col-sm-6">
            <BarChart />
          </div>
          <div className="col-sm-6">
            <DonutChart />
          </div>
        
        </div>

        <h1 className="text-primary">Tabela</h1>
        <DataTable />
      </div>
      <Footer />
    </>
  );
}

export default App;
